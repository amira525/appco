/*global $, document */
$(document).ready(function () {
    'use strict';
    $(".js-modal-btn").modalVideo();
    //
    
    //
    $('.navbar-nav li').click(function () {

        $('html, body').animate({

            scrollTop: $('#' + $(this).data('value')).offset().top

        }, 1000);

    });
    //
    $('.pricing .items .item').click(function () {
        $(this).toggleClass('active');
        $(this).parent().siblings().children().children('p').slideUp();
        $(this).children('p').slideToggle();
        $(this).parent().siblings().children().removeClass('active');
    });
    //slick-slider
    $('.main-slider').slick({
        rtl: document.dir === "rtl" ? true : false,
        arrows: false,
        autoPlay: true,
        autoplaySpeed: 1000,
        infinite: true,
        centerMode: true,
        centerPadding: '80px',
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '50px'
                }
            },
            {
                breakpoint: 600,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '40px'
                }
            },
            {
                breakpoint: 480,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '15px'
                }
            }
        ]
    });
    
    $('.slider-three').slick({
        rtl: document.dir === "rtl" ? true : false,
        arrows: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                slidesToShow: 3,
                slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 1
                }
            },
            {
                breakpoint: 300,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1
                }
            }
        ]
    });
    
    
    $('.frist-slider').slick({
        rtl: document.dir === "rtl" ? true : false,
        prevArrow: '<span class="slick-prev"><i class="fas fa-angle-left"></i></span>',
        nextArrow: '<span class="slick-next"><i class="fas fa-angle-right"></i></span>'
    });
});